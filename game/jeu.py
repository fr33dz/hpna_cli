# -*- coding: utf-8 -*-

class Jeu:
    def __init__(self, nom, age):
        self.nom = nom
        self.age = age

    def adulte(self):
        if self.age < 18:
            return False
        return True
    
    def nom_valide(self):
        if len(self.nom) < 3:
            return False
        return True

    def age_bonus(self, bonus):
        return self.age + bonus
