# -*- coding: utf-8 -*-

import os
import sys
import unittest

def config():
    DOSSIER_COURRANT = os.path.dirname(os.path.abspath(__file__))
    DOSSIER_PARENT = os.path.dirname(DOSSIER_COURRANT)
    sys.path.append(DOSSIER_PARENT)
    return True

class TestStringMethods(unittest.TestCase):
    
    def test_adulte(self):
        J = Jeu('omar', 27)
        K = Jeu('Nouna', 10)
        self.assertTrue(J.adulte())    
        self.assertFalse(K.adulte())

    def test_nom_valide(self):
        L = Jeu('Az', 26)
        M = Jeu('Athmane', 35)
        self.assertTrue(M.nom_valide())
        self.assertFalse(L.nom_valide())

    def test_age_bonus(self):
        K = Jeu('Nouna', 10)
        L = Jeu('Az', 26)
        self.assertEqual(K.age_bonus(10),20)
        self.assertEqual(L.age_bonus(4), 30)

if __name__ == '__main__':
    if config():
        from game.jeu import Jeu
        unittest.main()
